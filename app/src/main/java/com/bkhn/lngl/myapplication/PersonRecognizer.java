package com.bkhn.lngl.myapplication;


import android.graphics.Bitmap;
import android.util.Log;


import org.bytedeco.javacpp.Pointer;
import org.bytedeco.javacpp.opencv_contrib;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacpp.opencv_imgproc;
import org.bytedeco.javacv.AndroidFrameConverter;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.Java2DFrameConverter;
import org.bytedeco.javacv.OpenCVFrameConverter;
import org.opencv.android.JavaCamera2View;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;


import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import static org.bytedeco.javacpp.opencv_contrib.createLBPHFaceRecognizer;
import static org.bytedeco.javacpp.opencv_core.CV_32SC1;
import static org.bytedeco.javacpp.opencv_core.IPL_DEPTH_8U;
import static org.bytedeco.javacpp.opencv_highgui.CV_LOAD_IMAGE_GRAYSCALE;
import static org.bytedeco.javacpp.opencv_highgui.cvLoadImage;
import static org.bytedeco.javacpp.opencv_highgui.imread;
import static org.bytedeco.javacpp.opencv_imgproc.CV_BGR2GRAY;
import static org.bytedeco.javacpp.opencv_imgproc.cvCvtColor;
import static org.bytedeco.javacpp.opencv_imgproc.cvtColor;


public  class PersonRecognizer {

    opencv_contrib.FaceRecognizer faceRecognizer;
    String mPath;
    int count = 0;
    Labels labelsFile;

    static final int WIDTH = 128;
    static final int HEIGHT = 128;
    ;
    private int mProb = 999;


    PersonRecognizer(String path) {

        faceRecognizer = createLBPHFaceRecognizer(2, 8, 8, 8, 200);

        mPath = path;
        labelsFile = new Labels(mPath);


    }

    void add(Mat m, String description) {
        Bitmap bmp = Bitmap.createBitmap(m.width(), m.height(), Bitmap.Config.ARGB_8888);

        Utils.matToBitmap(m, bmp);
        bmp = Bitmap.createScaledBitmap(bmp, WIDTH, HEIGHT, false);

        FileOutputStream f;
        try {
            f = new FileOutputStream(mPath + description + "-" + count + ".jpg", true);
            count++;
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, f);
            f.close();

        } catch (Exception e) {
            Log.e("error", e.getCause() + " " + e.getMessage());
            e.printStackTrace();

        }
    }

    public boolean train() {

        File root = new File(mPath);

        FilenameFilter pngFilter = new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".jpg");

            }

            ;
        };

        File[] imageFiles = root.listFiles(pngFilter);

        opencv_core.MatVector images = new opencv_core.MatVector(imageFiles.length);



        opencv_core.Mat labels = new opencv_core.Mat(imageFiles.length, 1, CV_32SC1);
        int[] labelsBuffer = new int[imageFiles.length];

        int counter = 0;
        int label;

        opencv_core.Mat img;

        //opencv_core.Mat grayImg = new opencv_core.Mat();

        int i1 = mPath.length();


        for (File image : imageFiles) {
            String p = image.getAbsolutePath();
            img = imread(p, CV_LOAD_IMAGE_GRAYSCALE);

            if (img == null)
                Log.e("Error", "Error cVLoadImage");
            Log.i("image", p);

            int i2 = p.lastIndexOf("-");
            int i3 = p.lastIndexOf(".");
            int icount = Integer.parseInt(p.substring(i2 + 1, i3));
            if (count < icount) count++;

            String description = p.substring(i1, i2);

            if (labelsFile.get(description) < 0)
                labelsFile.add(description, labelsFile.max() + 1);

            label = labelsFile.get(description);




            images.put(counter, img);
            labelsBuffer[counter] = label;


            counter++;
        }
        labels.put(new opencv_core.Mat(labelsBuffer));

        if (counter > 0)
            if (labelsFile.max() > 1)

                faceRecognizer.train(images, labels);

        labelsFile.Save();
        return true;
    }

    public boolean canPredict() {
        if (labelsFile.max() > 1)
            return true;
        else
            return false;

    }

    public String predict(Mat m) {
        if (!canPredict())
            return "";
        int n[] = new int[1];
        double p[] = new double[1];



        opencv_core.IplImage ipl = MatToIplImage(m,WIDTH, HEIGHT);
        opencv_core.Mat mat = new opencv_core.Mat(ipl);

        faceRecognizer.predict(mat, n, p);


        if (n[0] != -1)
            mProb = (int) p[0];
        else
            mProb = -1;

        if (n[0] != -1)
            return labelsFile.get(n[0]);
        else
            return "Unknown";
    }








    opencv_core.IplImage MatToIplImage(Mat m, int width, int heigth)
    {


        Bitmap bmp=Bitmap.createBitmap(m.width(), m.height(), Bitmap.Config.ARGB_8888);


        Utils.matToBitmap(m, bmp);
        return BitmapToIplImage(bmp,width, heigth);

    }

    opencv_core.IplImage BitmapToIplImage(Bitmap bmp, int width, int height) {

        if ((width != -1) || (height != -1)) {
            Bitmap bmp2 = Bitmap.createScaledBitmap(bmp, width, height, false);
            bmp = bmp2;
        }

        opencv_core.IplImage image = opencv_core.IplImage.create(bmp.getWidth(), bmp.getHeight(),
                IPL_DEPTH_8U, 4);

        bmp.copyPixelsToBuffer(image.getByteBuffer());


        opencv_core.IplImage grayImg = opencv_core.IplImage.create(image.width(), image.height(),
                IPL_DEPTH_8U, 1);

        cvCvtColor(image, grayImg, opencv_imgproc.CV_BGR2GRAY);

        return grayImg;
    }



    protected void SaveBmp(Bitmap bmp,String path)
    {
        FileOutputStream file;
        try {
            file = new FileOutputStream(path , true);

            bmp.compress(Bitmap.CompressFormat.JPEG,100,file);
            file.close();
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e("",e.getMessage()+e.getCause());
            e.printStackTrace();
        }

    }



    public void load() {
        train();

    }

    public int getProb() {
        // TODO Auto-generated method stub
        return mProb;
    }


}